const jwt = require('jsonwebtoken')
const {users,results,choosens,rooms} = require('../models')


class utility {
   static getToken(username,permission) {
      const payload = {username : username, permission : permission}
      const AccessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET)
      console.log(AccessToken)
      return AccessToken
   }
   static async decodeToken(req,res,next) {
      const authHeader = req.headers.authorization
      const token = authHeader
      // console.log(token)
      if (!token) return res.status(401).json({
         msg: ` token not found`
      })
      jwt.verify(token, process.env.ACCESS_TOKEN_SECRET,(err, payload) => {
         if (err) return res.status(401).json({
            msg : `token not valid`
         })
         req.payload = payload
         const _username = payload.username
         const _permission = payload.permission
         console.log(`playing game for ${_username}, and authorize to ${_permission}`)
         req.username = _username
         next()
      })
   }

   

}

module.exports = {utility}