const jwt = require('jsonwebtoken')
const {users,results,choosens,rooms} = require('../models')


class utility {
   static getToken(username,permission) {
      const payload = {username : username, permission : permission}
      const AccessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET)
      console.log(AccessToken)
      return AccessToken
   }
   static async decodeToken(req,res,next) {
      const authHeader = req.headers.authorization
      const token = authHeader
      // console.log(token)
      if (!token) return res.status(401).json({
         msg: ` token not found`
      })
      jwt.verify(token, process.env.ACCESS_TOKEN_SECRET,(err, payload) => {
         if (err) return res.status(401).json({
            msg : `token not valid`
         })
         req.payload = payload
         const _username = payload.username
         const _permission = payload.permission
         console.log(`playing game for ${_username}, and authorize to ${_permission}`)
         req.username = _username
         next()
      })
   }

   static async result (req,res,next) {
      try {
         const {roomid} = req.params.id
         const dataplayer = await choosens.findOne({where: {RoomId: roomid}})
         console.log(dataplayer.player1)
         console.log(dataplayer.player2)
         const p1 = dataplayer.player1
         const p2 = dataplayer.player2
         if (!p1 || !p2) {
            console.log(`data has not been made`)
            res.json({
               msg: ` data has not been made`
            })
         }
         if(p1 = p2) {
            const result = await results.create({results:`draw`})
            console.log(result)
            console.log(result.results)
            res.json({
               msg: `p1 vs p2 is ${result.results}`
            })
         } else if((p1 = `R`) && (p2 = `P`)) {
            const result = await results.create({results:`p2 win`})
            console.log(result)
            console.log(result.results)
            res.json({
               msg: `p1 vs p2 is ${result.results}`
            })
         } else if((p1 = `P`) && (p2 = `R`)) {
            const result = await results.create({results:`p1 win`})
            console.log(result)
            console.log(result.results)
            res.json({
               msg: `p1 vs p2 is ${result.results}`
            })
         } else if((p1 = `R`) && (p2 = `S`)) {
            const result = await results.create({results:`p1 win`})
            console.log(result)
            console.log(result.results)
            res.json({
               msg: `p1 vs p2 is ${result.results}`
            })
         } else if((p1 = `S`) && (p2 = `R`)) {
            const result = await results.create({results:`p2 win`})
            console.log(result)
            console.log(result.results)
            res.json({
               msg: `p1 vs p2 is ${result.results}`
            })
         } else if((p1 = `S`) && (p2 = `P`)) {
            const result = await results.create({results:`p1 win`})
            console.log(result)
            console.log(result.results)
            res.json({
               msg: `p1 vs p2 is ${result.results}`
            })
         }else if((p1 = `P`) && (p2 = `S`)) {
            const result = await results.create({results:`p1 win`})
            console.log(result)
            console.log(result.results)
            res.json({
               msg: `p1 vs p2 is ${result.results}`
            })
         } else {
            console.log(`data not defined`)
            res.json({
               msg: `data is not defined`
            })
         }
         
      } catch (error) {
         console.log(`err`, error)
         res.status(400).json({
            msg: `err ${error}`
         })
      }
   }

}

module.exports = {utility}