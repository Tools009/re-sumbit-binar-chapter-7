'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class choosens extends Model {

    static associate(models) {
      choosens.belongsTo(models.rooms, {foreignKey: 'RoomId'})
    }
  }
  choosens.init({
    RoomId: DataTypes.INTEGER,
    player1: DataTypes.STRING,
    player2: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'choosens',
  });
  return choosens;
};